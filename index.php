

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://bossinternational.asia/" />
<link rel="canonical" href="https://bossinternational.asia/" />
<meta property="og:title" content="Crypto Square Merchant Event" />
<title>Crypto Square Merchant Event</title>
<meta property="og:description" content="Crypto Square Merchant Event - The virtual fair of cryptocurrency." />
<meta name="description" content="Crypto Square Merchant Event - The virtual fair of cryptocurrency." />
<meta name="keywords" content="Livestream,Crypto Square, virtual fair, online, video, live, etc">

<?php include 'css.php'; ?>
</head>

<body>



<div class="width100 same-padding overflow gold-bg min-height">

    <div class="width100 overflow margin-top30">
    	<div class="width100 overflow text-center">
    		<img src="img/crypto-square.png" class="guangming-logo" alt="Crypto Square" title="Crypto Square">
		</div>
        <h1 class="title-h1 text-center landing-title-h1">Crypto Square Merchant Event</h1>
        <!-- <h1 class="title-h1 text-center landing-title-h1 black-text"><?php //echo $mainTitle->getName();?></h1> -->
		<div class="width100 overflow">
                <iframe class="landing-top-iframe" src="https://www.youtube.com/embed/PxNwygbVamA?&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
       <!-- <?php
        if($adminAutoplay == "Yes")
        {
        ?>
            <div class="width100 overflow">
                <iframe class="landing-top-iframe" src="https://www.youtube.com/embed/<?php echo $adminLink;?>?&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="width100 overflow">
                <iframe class="landing-top-iframe" src="https://www.youtube.com/embed/<?php echo $adminLink;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        <?php
        }
        ?>-->

        <div class="two-section-container overflow">
                        <a href='coinbase.php' target="_blank">
                            <div class="two-section-div opacity-hover">
                                <p class="subtitle-p gold-text">Coinbase</p> 
                                        <iframe class="two-section-iframe" src="https://www.youtube.com/embed/R7pFVzOvUiw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <div class="clear"></div>
                                        <a href='coinbase.php' target="_blank"><div class="guang-button">View More</div></a>
                            </div>
                        </a>        
                        <a href='coingeek.php' target="_blank">
                            <div class="two-section-div opacity-hover">
                                <p class="subtitle-p gold-text">CoinGeek</p> 
                                        <iframe class="two-section-iframe" src="https://www.youtube.com/embed/UXvssVNM_rw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <div class="clear"></div>
                                        <a href='coingeek.php' target="_blank"><div class="guang-button">View More</div></a>
                            </div>
                        </a>         
       <!-- 
        <?php
        $conn = connDB();
        if($liveDetails)
        {
            for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
            {
            ?>

                <?php 
                    $platfrom =  $liveDetails[$cnt]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
                            <div class="two-section-div opacity-hover">
                                <p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                        <iframe class="two-section-iframe" src="https://www.youtube.com/embed/<?php echo $liveDetails[$cnt]->getLink();?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <div class="clear"></div>
                                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>
                    <?php
                    }

                    elseif($platfrom == 'Zoom')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                <div class="two-section-iframe background-css" id="z<?php echo $liveDetails[$cnt]->getUid();?>" value="<?php echo $liveDetails[$cnt]->getUid();?>">
                            	</div>
                                <div class="clear"></div>
                                <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
							</div>
                        </a>
                        
						<style>
                        	#z<?php echo $liveDetails[$cnt]->getUid();?>{
								background-image:url(userProfilePic/<?php echo $liveDetails[$cnt]->getBroadcastShare();?>);}
                        </style>
                    <?php
                    }

                    elseif($platfrom == 'Facebook')
                    {
                    ?>

                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p  class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 

   
                                    <iframe  class="two-section-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $liveDetails[$cnt]->getLink();?>"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                           			<div class="clear"></div>
                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>

                    <?php
                    }

                    else
                    {   }
                ?>

            <?php
            }
            ?>

        <?php
        }

        else
        {
        ?>
            NO BROADCASTING AT THE MOMENT, WE WILL RETURN SOON !!
        <?php
        }

        ?>-->
    </div>
    </div>

    <div class="clear"></div>
    
</div>

<?php include 'js.php'; ?>

</body>
</html>