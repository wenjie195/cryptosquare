<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $liveDetails = getLiveShare($conn);
// $subDetails = getSubShare($conn);

// $liveDetails = getLiveShare($conn," WHERE status != 'Available' AND type = '1' ");
// $subDetails = getSubShare($conn," WHERE status != 'Available' AND type = '1' ");

$liveDetails = getLiveShare($conn," WHERE status != 'Delete' ");
$subDetails = getSubShare($conn," WHERE status != 'Delete' ");

$allUser = getUser($conn," WHERE user_type = '1' ");
$allLive = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="All Video | Property" />
<title>All Video | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">
   


        <?php
            if(isset($_POST['data_uid']))
            {
                $conn = connDB();
                $liveDetails = getLiveShare($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['data_uid']),"s");
            ?>

                <h2 class="h1-title">Main Video</h2>    
                <div class="clear"></div>
                <div class="scroll-div margin-top30">                    
                    <table class="table-css">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Title</th>

                                    <th>Added On</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Action</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if($liveDetails)
                                {
                                    for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
                                    {
                                    ?>    
                                        <tr>
                                            <td><?php echo ($cnt+1)?></td>

                                            <td><?php echo $liveDetails[$cnt]->getTitle();?></td>

                                            <td>
                                                <?php echo $date = date("d-m-Y",strtotime($liveDetails[$cnt]->getDateCreated()));?>
                                            </td>
                                            <td><?php echo $liveDetails[$cnt]->getStatus();?></td>

                                            <td>
                                                <form action="editLive.php" method="POST" class="hover1">
                                                    <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                        Edit
                                                    </button>
                                                </form> 
                                            </td>


                                                <?php
                                                    $liveStatus = $liveDetails[$cnt]->getStatus();
                                                    if($liveStatus == 'Available')
                                                    {
                                                    ?>
											<td>
                                                        <form method="POST" action="utilities/stopLiveFunction.php" class="hover1">
                                                            <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                                Hide
                                                            </button>
                                                        </form>
											</td>
                                                    <?php
                                                    }
                                                    elseif($liveStatus == 'Stop')
                                                    {
                                                    ?>
											<td>
                                                        <form method="POST" action="utilities/startLiveFunction.php" class="hover1">
                                                            <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                                Show
                                                            </button>
                                                        </form>
											</td>
                                                    <?php
                                                    }

                                                    elseif($liveStatus == 'Pending')
                                                    {
                                                    ?>
												<td>
                                                        <form method="POST" action="utilities/startLiveFunction.php" class="hover1">
                                                            <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                                Show
                                                            </button>
                                                        </form>
												</td>
                                                    <?php
                                                    }

                                                ?>
                                            <td>
                                                <form method="POST" action="utilities/deleteLiveFunction.php" class="hover1">
                                                    <button class="clean action-button" type="submit" name="data_id" value="<?php echo $liveDetails[$cnt]->getId();?>">
                                                        Delete
                                                    </button>
                                                </form>
											</td>                                            

                                        </tr>
                                    <?php
                                    }
                                }
                                ?>                                 
                            </tbody>
                    </table>
				</div>
            <?php
            }
        ?>

        <?php
            if(isset($_POST['data_uid']))
            {
                $conn = connDB();
                $subDetails = getSubShare($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['data_uid']),"s");
            ?>
			<div class="clear"></div>
                <h2 class="h1-title">Project Video</h2>    
                <div class="clear"></div>
                <div class="scroll-div margin-top30">                    
                    <table class="table-css">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Title</th>

                                    <th>Video 1</th>
                                   <!-- <th>Platform 1</th>
                                    <th>Link 1</th>
                                    <th>Remark 1</th>-->

                                    <th>Video 2</th>
                                    <!--<th>Platform 2</th>
                                    <th>Link 2</th>
                                    <th>Remark 2</th>-->

                    
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Action</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if($subDetails)
                                {
                                    for($cntAA = 0;$cntAA < count($subDetails) ;$cntAA++)
                                    {
                                    ?>    
                                        <tr>
                                            <td><?php echo ($cntAA+1)?></td>
                                            <td><?php echo $subDetails[$cntAA]->getTitle();?></td>

                                            <td><?php echo $subDetails[$cntAA]->getHost();?></td>
                                            <!--<td><?php echo $subDetails[$cntAA]->getPlatform();?></td>
                                            <td><?php echo $subDetails[$cntAA]->getLink();?></td>
                                            <td><?php echo $subDetails[$cntAA]->getRemark();?></td>-->

                                            <td><?php echo $subDetails[$cntAA]->getHostTwo();?></td>
                                            <!--<td><?php echo $subDetails[$cntAA]->getPlatformTwo();?></td>
                                            <td><?php echo $subDetails[$cntAA]->getLinkTwo();?></td>
                                            <td><?php echo $subDetails[$cntAA]->getRemarkTwo();?></td>-->

                                            <!-- <td>
                                                <?php //echo $date = date("d-m-Y",strtotime($subDetails[$cntAA]->getDateCreated()));?>
                                            </td> -->

                                            <td><?php echo $subDetails[$cntAA]->getStatus();?></td>

                                            <td>
                                                <form action="editSub.php" method="POST" class="hover1">
                                                    <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                        Edit
                                                    </button>
                                                </form> 
                                            </td>


                                                <?php
                                                    $status = $subDetails[$cntAA]->getStatus();
                                                    if($status == 'Available')
                                                    {
                                                    ?>
													<td>
                                                        <form method="POST" action="utilities/stopSubFunction.php" class="hover1">
                                                            <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                                Stop
                                                            </button>
                                                        </form>
													</td>
                                                    <?php
                                                    }
                                                    elseif($status == 'Stop')
                                                    {
                                                    ?>
													<td>
                                                        <form method="POST" action="utilities/startSubFunction.php" class="hover1">
                                                            <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                                Start
                                                            </button>
                                                        </form>
													</td>
                                                    <?php
                                                    }
                                                    elseif($status == 'Pending')
                                                    {
                                                    ?>
													<td>
                                                        <form method="POST" action="utilities/startSubFunction.php" class="hover1">
                                                            <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                                Start
                                                            </button>
                                                        </form>
													</td>
                                                    <?php
                                                    }

                                                ?>
                                       
                                            <td>
                                                <form method="POST" action="utilities/deleteSubFunction.php" class="hover1">
                                                    <button class="clean action-button" type="submit" name="subdata_id" value="<?php echo $subDetails[$cntAA]->getId();?>">
                                                        Delete
                                                    </button>
                                                </form>
											</td>
                                        </tr>
                                    <?php
                                    }
                                }
                                ?>                                 
                            </tbody>
                    </table>

            <?php
            }
        ?>


	</div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>