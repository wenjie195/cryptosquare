<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Liveshare.php';
// require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Home Main Video | Property" />
<title>Home Main Video  | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

	<h2 class="h1-title">Home Main Video <img src="img/note.png" class="opacity-hover icon-png open-homevideo" alt="What is it?" title="What is it?"></h2> 
        <div class="clear"></div>

        <form method="POST" action="utilities/editAdminBroadcastFunction.php">

            <div class="width100">
                <p class="input-top-text">Link</p>
                <input class="aidex-input clean" type="text" value="<?php echo $userData->getLink();?>" name="update_link" id="update_link" required>       
            </div>
    	
            <div class="clear"></div>  

           <div class="dual-input">
                <p class="input-top-p admin-top-p">Autoplay</p>
                <select class="aidex-input clean" type="text" name="autoplay_function" id="autoplay_function">
                    <?php
                        if($userData->getAutoplay() == '')
                        {
                        ?>
                            <option value="No"  name='No'>No</option>
                            <option value="Yes"  name='Yes'>Yes</option>
                            <option selected value=""  name=''>Please Select An Option</option>
                        <?php
                        }
                        else if($userData->getAutoplay()  == 'Yes')
                        {
                        ?>
                            <option value="No"  name='No'>No</option>
                            <option selected value="Yes"  name='Yes'>Yes</option>
                        <?php
                        }
                        else if($userData->getAutoplay()  == 'No')
                        {
                        ?>
                            <option selected value="No"  name='No'>No</option>
                            <option value="Yes"  name='Yes'>Yes</option>
                    <?php
                    }
                    ?>
                </select> 

            </div>

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

	</div>
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>