<!--<//?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(2),"s");
// $liveDetails = getLiveShare($conn," WHERE username = 'MahSing' AND status = 'Available' AND type = '1' ");
// $subDetails = getSubShare($conn," WHERE username = 'MahSing' AND status = 'Available' AND type = '1' ");

// $zoomDetails = getSubShare($conn," WHERE platform = 'Zoom' ");

// $subDetails = getSubShare($conn," WHERE status = 'Available' AND type = '1' ");

// $mainLive = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");
// $mainSub = getUser($conn," WHERE broadcast_share = 'Available' AND user_type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>-->

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://bossinternational.asia/coingeek.php" />
<link rel="canonical" href="https://bossinternational.asia/coingeek.php" />
<meta property="og:title" content="CoinGeek | Crypto Square Merchant Event" />
<title>CoinGeek | Crypto Square Merchant Event</title>
<meta property="og:description" content="CoinGeek | Crypto Square Merchant Event - The virtual fair of cryptocurrency." />
<meta name="description" content="CoinGeek | Crypto Square Merchant Event - The virtual fair of cryptocurrency." />
<meta name="keywords" content="Livestream,Crypto Square, virtual fair, online, video, live, etc">

<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>

<div class="width100 same-padding overflow gold-bg min-height">
<!--
    <//?php
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
    $link = "https";
    else
    $link = "http";

    // Here append the common URL characters.
    $link .= "://";

    // Append the host(domain name, ip) to the URL.
    $link .= $_SERVER['HTTP_HOST'];

    // Append the requested resource location to the URL
    $link .= $_SERVER['REQUEST_URI'];


    if(isset($_GET['id']))
    {
        $referUidLink = $_GET['id'];
    }
    else
    {
        $referUidLink = "";
    }
    ?>-->

    <div class="width100 overflow margin-top30 first-div-margin">
<!--
        <//?php
        $conn = connDB();

        $liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
        // $title = $liveDetails[0]->getUsername(); 

        if($liveDetails)
        {
            for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
            {
            ?>
            
                <//?php 
                    $platfrom =  $liveDetails[$cnt]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>-->

                        <div class="width100 top-video-div overflow">
                            <div class="left-video-div">
                                <iframe class="youtube-top-iframe" src="https://www.youtube.com/embed/UXvssVNM_rw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="right-project-div">
                                <a href="https://bitcoin.org/" target="_blank"><img src="img/logo-01.png" class="first-logo project-logo opacity-hover"></a>
                                <a href="https://www.binance.com/" target="_blank"><img src="img/logo-07.png" class="second-logo project-logo opacity-hover"></a>
                 
                                <a href="https://litecoin.org/" target="_blank"><img src="img/logo-03.png" class="first-logo project-logo opacity-hover"></a>
                                <a href="https://tether.to/" target="_blank"><img src="img/logo-04.png" class="second-logo project-logo opacity-hover"></a>
                                <a href="https://www.bitcoincash.org/" target="_blank"><img src="img/logo-05.png" class="first-logo project-logo opacity-hover"></a>
                                <a href="https://libra.org/" target="_blank"><img src="img/logo-06.png" class="second-logo project-logo opacity-hover "></a>
                                <a href="https://ethereum.org/" target="_blank"><img src="img/logo-02.png" class="first-logo project-logo opacity-hover ow-margin-bottom0"></a>
                                <a href="https://www.peercoin.net/" target="_blank"><img src="img/logo-08.png" class="second-logo project-logo opacity-hover ow-margin-bottom0"></a>                
                            </div>
                        </div>

<!--
                    <//?php
                    }
                    else
                    {   }
                ?>

            <//?php
            }
            ?>
        <//?php
        }
        ?>-->
    
    </div>

    <div class="clear"></div>
    
    <div class="width100 overflow margin-top30">
		<div class="left-video-container-div overflow">
                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/6M4O5Tp5D5Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <p class="gold-text four-div-p"><b><a href="https://coingeek.com/" class="blue-to-orange" target="_blank">Learn More</a></b></p>
                        </div>     
                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/133J3nGze1A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <p class="gold-text four-div-p"><b><a href="https://coingeek.com/" class="blue-to-orange" target="_blank">Learn More</a></b></p>
                        </div>                          
                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/jJ4kn1YvoZY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <p class="gold-text four-div-p"><b><a href="https://coingeek.com/" class="blue-to-orange" target="_blank">Learn More</a></b></p>
                        </div>                          
                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/6eFWSFK0nLs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <p class="gold-text four-div-p"><b><a href="https://coingeek.com/" class="blue-to-orange" target="_blank">Learn More</a></b></p>
                        </div>                             
        </div>
    	<div class="left-video-container-div overflow">
                        <div class="staff-div-css overflow opacity-hover">
                            <a href="https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09#success" target="_blank">
                                <div class="four-div-iframe staff-1"></div>
                                <p class="gold-text four-div-p text-overflow"><b>Consultant</b><br>John Smith</p>
                            </a>
                        </div>         	
                        <div class="staff-div-css overflow opacity-hover">
                            <a href="https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09#success" target="_blank">
                                <div class="four-div-iframe staff-2"></div>
                                <p class="gold-text four-div-p text-overflow"><b>HOD of Sales Dept</b><br>Angie Lina</p>
                            </a>
                        </div> 
                        <div class="staff-div-css overflow opacity-hover">
                            <a href="https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09#success" target="_blank">
                                <div class="four-div-iframe staff-3"></div>
                                <p class="gold-text four-div-p text-overflow"><b>Sales Rep. 1</b><br>James Jim</p>
                            </a>
                        </div>                         
                        <div class="staff-div-css overflow opacity-hover">
                            <a href="https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09#success" target="_blank">
                                <div class="four-div-iframe staff-4"></div>
                                <p class="gold-text four-div-p text-overflow"><b>Sales Rep. 2</b><br>Jack Nox</p>
                            </a>
                        </div>                         
                        
                        <div class="staff-div-css overflow opacity-hover">
                            <a href="https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09#success" target="_blank">
                                <div class="four-div-iframe staff-5"></div>
                                <p class="gold-text four-div-p text-overflow"><b>Sales Rep. 3</b><br>Jimmy Lox</p>
                            </a>
                        </div>                         
                        
                                
        </div>
    </div>

    </div>

    <div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>