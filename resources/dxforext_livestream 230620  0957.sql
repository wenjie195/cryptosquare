-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 23, 2020 at 09:53 AM
-- Server version: 5.7.30
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dxforext_livestream`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` text,
  `seo_title` text,
  `article_link` text,
  `keyword_one` text,
  `keyword_two` text,
  `title_cover` text,
  `paragraph_one` text,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text,
  `img_one_source` text,
  `img_two_source` text,
  `img_three_source` text,
  `img_four_source` text,
  `img_five_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` bigint(20) NOT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` int(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `live_share`
--

CREATE TABLE `live_share` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `live_share`
--

INSERT INTO `live_share` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'd6df95992f6888098f4e50a8cafa464b', '79968f3ed6c151199d3f12588d11d20f', 'MahSing', 'New Handover Journey Begins at Avens, Southville City', 'MahSing', 'Youtube', 'P11B-6eWFKc', 'Available', 1, '2020-06-19 07:54:37', '2020-06-19 07:54:37'),
(2, '31d70925c77da1ec8640d7de9cabf4c5', '8f769dd5dcdbbf843513217c2e4585ed', 'Eco World', 'Eco World for Generation', 'Eco World Admin', 'Youtube', 'BHIzD4XSPAU', 'Available', 1, '2020-06-22 09:33:50', '2020-06-22 09:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

CREATE TABLE `platform` (
  `id` bigint(20) NOT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` (`id`, `platform`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Youtube', 'Available', 1, '2020-04-21 08:00:50', '2020-04-21 08:00:50'),
(2, 'Facebook', 'Available', 1, '2020-04-21 08:01:55', '2020-04-21 08:01:55'),
(3, 'Zoom', 'Available', 1, '2020-04-21 08:05:41', '2020-04-21 08:05:41'),
(4, 'UStream', 'Available', 1, '2020-04-21 08:05:51', '2020-04-21 08:05:51');

-- --------------------------------------------------------

--
-- Table structure for table `sub_share`
--

CREATE TABLE `sub_share` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'video_uid',
  `user_uid` varchar(255) DEFAULT NULL COMMENT 'user_uid',
  `username` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `host` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `link` text,
  `remark` text,
  `host_two` varchar(255) DEFAULT NULL,
  `platform_two` varchar(255) DEFAULT NULL,
  `link_two` text,
  `remark_two` text,
  `status` varchar(255) DEFAULT NULL,
  `type` int(5) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_share`
--

INSERT INTO `sub_share` (`id`, `uid`, `user_uid`, `username`, `title`, `host`, `platform`, `link`, `remark`, `host_two`, `platform_two`, `link_two`, `remark_two`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, '83b756f2bbd0db31819a1078684a7acc', '8f769dd5dcdbbf843513217c2e4585ed', 'Eco World', 'Eco Majestic - Mellowood Precinct Video', 'Eco World', 'Youtube', 'PveDiXt7CQc', NULL, 'asd', 'Youtube', 'XaUdOiFhY0A', NULL, 'Available', 1, '2020-06-19 08:11:19', '2020-06-19 08:11:19'),
(2, '2471185e7fd0d34a318517484786c10f', '79968f3ed6c151199d3f12588d11d20f', 'MahSing', 'Home Begins Here', 'Admin Mah Sing', 'Youtube', 'JWOiZDaSU0w', '3Q', '', 'Youtube', 't3mEr5y-Ul0', '3Q', 'Available', 1, '2020-06-22 09:41:00', '2020-06-22 09:41:00'),
(3, '9cd859b6e809f7e16b53273ad0234f83', '79968f3ed6c151199d3f12588d11d20f', 'MahSing', 'Consultant', 'James Lim', 'Zoom', 'https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09', 'Consultant', 'Joyce Ang', 'Zoom', 'https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09', 'HOD of Sales Dept.', 'Available', 1, '2020-06-19 08:16:20', '2020-06-19 08:16:20'),
(6, '08f32b23604d20fc8c573e8aeac1dd7c', '8f769dd5dcdbbf843513217c2e4585ed', 'Eco World', 'Eco World Live Sharing', 'Eco Ng', 'Zoom', 'https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09', 'Consultant', 'Eco Lim', 'Zoom', 'https://us04web.zoom.us/j/78644537230?pwd=L1hUYi8zbnozNS9kWWJqMFh6UWs4Zz09', 'HOD of Sales Dept.', 'Available', 1, '2020-06-22 09:53:30', '2020-06-22 09:53:30');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `broadcast_live` varchar(255) DEFAULT NULL COMMENT 'Yes = On Live, No = Off Live',
  `title` varchar(255) DEFAULT NULL,
  `broadcast_share` varchar(255) DEFAULT NULL COMMENT 'Yes = On Live, No = Off Live',
  `platform` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `autoplay` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT '1' COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `phone_no`, `full_name`, `nationality`, `broadcast_live`, `title`, `broadcast_share`, `platform`, `link`, `autoplay`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9a04ea5286dc214d621622e14a858dc4', 'admin', 'admin@gmail.com', '2bd3fc76b60d220e67a40adb4c4a9be1d2cdde2afb4a406b6ff1c2daad40ea59', 'a221197fcfb97258210864e1175f32f221cdc16f', '6012-3456789', NULL, NULL, NULL, NULL, NULL, 'Youtube', 'zpH9UZrU6h4', 'Yes', 1, 0, '2020-03-18 06:28:23', '2020-06-23 01:51:59'),
(2, '8f769dd5dcdbbf843513217c2e4585ed', 'Eco World', 'corp@ecoworld.my', '59c58e2999e1c6345e6b729570168bba13448f62817e8fbe4c43c287d9c1eaa9', '7667902f98f1ae82a337671613845953b9ba8379', '0333442552', NULL, NULL, 'Available', NULL, NULL, 'Youtube', 'BHIzD4XSPAU', NULL, 1, 1, '2020-06-19 07:42:07', '2020-06-22 08:07:42'),
(3, '79968f3ed6c151199d3f12588d11d20f', 'MahSing', 'crm@mahsing.com.my', '62a5c04871685be39a82b691d0d17d10fd5ad25ed04f437ddc8efac33ceff9ff', 'df0b5d2d1057fd6602cffd3b5774fd5c475233f7', '1300806888', NULL, NULL, 'Available', NULL, '1592819336', 'Youtube', 'P11B-6eWFKc', NULL, 1, 1, '2020-06-19 07:45:43', '2020-06-22 09:48:56'),
(4, '81f4f37ed4625ec54f965de082e42ca0', 'BSG Property', 'bsgtb-care@bsg.com.my', '1638f49ab361256fef53629d7ae7428a0ceba6af5c32c672af810936234c6b97', '774ca6ea16a021403407c5e31460c72ef85da910', '048912828', NULL, NULL, 'Stop', NULL, NULL, 'Youtube', 'sdTGynm1NPc', NULL, 1, 1, '2020-06-19 07:47:05', '2020-06-22 09:49:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `live_share`
--
ALTER TABLE `live_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `platform`
--
ALTER TABLE `platform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_share`
--
ALTER TABLE `sub_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `live_share`
--
ALTER TABLE `live_share`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `platform`
--
ALTER TABLE `platform`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sub_share`
--
ALTER TABLE `sub_share`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
