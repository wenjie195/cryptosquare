<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(2),"s");
// $liveDetails = getLiveShare($conn," WHERE status = 'Available' AND type = '1' ");
// $subDetails = getSubShare($conn," WHERE status = 'Available' AND type = '1' ");

$mainLive = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");
$mainSub = getUser($conn," WHERE broadcast_share = 'Available' AND user_type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Index | Livestream" />
<title>Index | Livestream</title>
<meta property="og:description" content="Livestream" />
<meta name="description" content="Livestream" />
<meta name="keywords" content="Livestream">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body>


<div class="background-div">

    <form action="utilities/loginFunction.php" method="POST">
        <input class="clean de-input" type="text" placeholder="Username" id="username" name="username" required>
        <input class="clean de-input" type="password" placeholder="Password" id="password" name="password" required>
        <div class="clear"></div>
        <button class="clean blue-button mid-button-width small-distance small-distance-bottom" name="login">Login</button>
    </form>

    <div class="cover-gap content min-height2">

        <?php
        if($mainLive)
        {
            for($cnt = 0;$cnt < count($mainLive) ;$cnt++)
            {
            ?>

                <?php 
                    $platfrom =  $mainLive[$cnt]->getPlatform();
                    if($platfrom == 'Facebook')
                    {
                    ?>

                    <?php
                    }
                    elseif($platfrom == 'Youtube')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $mainLive[$cnt]->getUid();?>'>
                            <div class="shadow-white-box product-box opacity-hover">

                                <div class="article-card article-card-overwrite">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $mainLive[$cnt]->getLink();?>?&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="youtube-video-iframe" allowfullscreen></iframe>
                                </div>

                                <a href='uniqueIndex.php?id=<?php echo $mainLive[$cnt]->getUid();?>' target="_blank">
                                    <p>Click for more</p>
                                    <p><?php echo $mainLive[$cnt]->getUid();?></p>
                                    <!-- <p><?php echo $mainLive[$cnt]->getUsername();?></p> -->
                                </a>

                            </div>
                        </a>
                    <?php
                    }
                ?>

            <?php
            }
            ?>
        <?php
        }
        ?>

        <div class="clear"></div>

    </div>

</div>

</body>
</html>