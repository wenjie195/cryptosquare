<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(2),"s");
// $liveDetails = getLiveShare($conn," WHERE username = 'MahSing' AND status = 'Available' AND type = '1' ");
// $subDetails = getSubShare($conn," WHERE username = 'MahSing' AND status = 'Available' AND type = '1' ");

// $zoomDetails = getSubShare($conn," WHERE platform = 'Zoom' ");

// $subDetails = getSubShare($conn," WHERE status = 'Available' AND type = '1' ");

// $mainLive = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");
// $mainSub = getUser($conn," WHERE broadcast_share = 'Available' AND user_type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Property" />
<title>Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">

<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>

<div class="width100 same-padding overflow gold-bg min-height">

    <?php
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
    $link = "https";
    else
    $link = "http";

    // Here append the common URL characters.
    $link .= "://";

    // Append the host(domain name, ip) to the URL.
    $link .= $_SERVER['HTTP_HOST'];

    // Append the requested resource location to the URL
    $link .= $_SERVER['REQUEST_URI'];


    if(isset($_GET['id']))
    {
        $referUidLink = $_GET['id'];
    }
    else
    {
        $referUidLink = "";
    }
    ?>

    <div class="width100 overflow margin-top30 first-div-margin">

        <?php
        $conn = connDB();

        $liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
        // $title = $liveDetails[0]->getUsername(); 

        if($liveDetails)
        {
            for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
            {
            ?>
            
                <?php 
                    $platfrom =  $liveDetails[$cnt]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>

                        <div class="width100 top-video-div overflow">
                            <div class="left-video-div">
                                <iframe class="youtube-top-iframe" src="https://www.youtube.com/embed/<?php echo $liveDetails[$cnt]->getLink();?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="right-project-div">
                                <a href="https://www.cityofdreams.net.my/" target="_blank"><img src="img/cod.png" class="first-logo project-logo opacity-hover"></a>
                                <!--<a href="https://ecoworld.my/ecomajestic/" target="_blank"><img src="img/logo-01.png" class="first-logo project-logo opacity-hover"></a>-->
                                <a href="https://www.eweinzenith.com/" target="_blank"><img src="img/ewein.png" class="second-logo project-logo opacity-hover"></a>
                                <a href="https://ecoworld.my/ecoforest/" target="_blank"><img src="img/logo-03.png" class="first-logo project-logo opacity-hover"></a>
                                <a href="https://ecoworld.my/ecohorizon/" target="_blank"><img src="img/logo-04.png" class="second-logo project-logo opacity-hover"></a>
                                <a href="https://ecoworld.my/ecobotanic/" target="_blank"><img src="img/logo-05.png" class="first-logo project-logo opacity-hover"></a>
                                <a href="https://ecoworld.my/ecotropics/" target="_blank"><img src="img/logo-06.png" class="second-logo project-logo opacity-hover "></a>
                                <a href="https://ecoworld.my/ecobusinesspark2/" target="_blank"><img src="img/logo-07.png" class="first-logo project-logo opacity-hover ow-margin-bottom0"></a>
                                <a href="https://ecoworld.my/ecosky/" target="_blank"><img src="img/logo-08.png" class="second-logo project-logo opacity-hover ow-margin-bottom0"></a>                
                            </div>
                        </div>


                    <?php
                    }
                    else
                    {   }
                ?>

            <?php
            }
            ?>
        <?php
        }
        ?>
    
    </div>

    <div class="clear"></div>
    
    <div class="width100 overflow margin-top30 four-div-big-div">

        <?php
        $conn = connDB();
        $subDetails = getSubShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
        if($subDetails)
        {
            for($cntAA = 0;$cntAA < count($subDetails) ;$cntAA++)
            {
            ?>

                <?php 
                    $platfrom =  $subDetails[$cntAA]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>

                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/<?php echo $subDetails[$cntAA]->getLink();?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <p class="gold-text four-div-p"><b>Live Tour</b></p>
                        </div>

                        <div class="four-div overflow second-four-div">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/<?php echo $subDetails[$cntAA]->getLinkTwo();?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <p class="gold-text four-div-p"><b>Live Tour</b></p>
                        </div>

                    <?php
                    }
                    elseif($platfrom == 'Zoom')
                    {
                    ?>

                        <!-- <div class="four-div overflow opacity-hover"> -->
                        <div class="four-div overflow third-four-div opacity-hover">
                            <a href="<?php echo $subDetails[$cntAA]->getLink();?>" target="_blank">
                                <div class="four-div-iframe staff-1"></div>
                                <p class="gold-text four-div-p"><b><?php echo $subDetails[$cntAA]->getRemark();?></b><br><?php echo $subDetails[$cntAA]->getHost();?></p>
                            </a>
                        </div>  

                        <!-- <div class="four-div overflow opacity-hover"> -->
                        <!-- <div class="four-div overflow third-four-div opacity-hover"> -->
                        <div class="four-div overflow opacity-hover">
                            <a href="<?php echo $subDetails[$cntAA]->getLinkTwo();?>" target="_blank">
                                <div class="four-div-iframe staff-2"></div>
                                <p class="gold-text four-div-p"><b><?php echo $subDetails[$cntAA]->getRemarkTwo();?></b><br><?php echo $subDetails[$cntAA]->getHostTwo();?></p>
                            </a>
                        </div>  

                    <?php
                    }
                    else
                    {   }
                ?>

            <?php
            }
            ?>

            <!-- <form action="liveChat.php" method="POST" class="hover1">
                <button class="clean action-button" type="submit" name="user_uid" value="<?php //echo $referUidLink;?>">
                    Live Chat
                </button>
            </form>  -->

        <?php
        }
        ?>

    </div>

    <div class="clear"></div>

</div>

<?php include 'js.php'; ?>

</body>
</html>