<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$allUser = getUser($conn," WHERE user_type = '1' ");
$platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add New Project Video | Property" />
<title>Add New Project Video | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>
<div class="width100 same-padding overflow gold-bg min-height-footer-only">

	<h2 class="h1-title">Add New Project Video</h2> 
    <div class="clear"></div>
        <form action="utilities/registerNewSubFunction.php" method="POST"> 

            <div class="dual-input">
                <p class="input-top-text">User</p>  
                <select class="aidex-input clean" type="text" name="register_user" id="register_user" required>
                    <option value="">Please Select A User</option>
                    <?php
                    for ($cnt=0; $cnt <count($allUser) ; $cnt++)
                    {
                    ?>
                        <option value="<?php echo $allUser[$cnt]->getUsername(); ?>"> 
                            <?php echo $allUser[$cnt]->getUsername(); ?>
                        </option>
                    <?php
                    }
                    ?>
                </select>  
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Project Title</p>
                <input class="aidex-input clean" type="text" placeholder="Title" name="update_title" id="update_title" required>       
            </div>
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-text">Host 1</p>
                <input class="aidex-input clean" type="text" placeholder="Host Name" name="update_host" id="update_host" required>       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Platform</p>
                <select class="aidex-input clean" type="text" name="register_platform" id="register_platform" required>
                    <option value="">Please Select A Platform</option>
                    <?php
                    for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                    {
                    ?>
                        <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                            <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                        </option>
                    <?php
                    }
                    ?>
                </select> 

            </div>
			<div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-text">Link</p>
                <input class="aidex-input clean" type="text" placeholder="Link" name="register_link" id="register_link" required>       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Remark</p>
                <input class="aidex-input clean" type="text" placeholder="Remark" name="register_remark" id="register_remark" required>       
            </div>
			<div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-text">Host 2</p>
                <input class="aidex-input clean" type="text" placeholder="Host Name" name="register_host_two" id="register_host_two">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Platform</p>
                <select class="aidex-input clean" type="text" name="register_platform_two" id="register_platform_two">
                    <option value="">Please Select A Platform</option>
                    <?php
                    for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                    {
                    ?>
                        <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                            <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                        </option>
                    <?php
                    }
                    ?>
                </select> 

            </div>
			<div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-text">Link</p>
                <input class="aidex-input clean" type="text" placeholder="Link" name="register_link_two" id="register_link_two">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Remark</p>
                <input class="aidex-input clean" type="text" placeholder="Remark" name="register_remark_two" id="register_remark_two">       
            </div>
        
            <div class="clear"></div> 

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
            </div>

        </form>
    </div>
    
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>