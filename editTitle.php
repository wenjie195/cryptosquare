<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Title.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$platformTitle = getTitle($conn);
$mainTitle = $platformTitle[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Title | Property" />
<title>Edit Title  | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Edit Title</h2> 

    <form method="POST" action="utilities/editTitleFunction.php">

    <div class="dual-input">
        <p class="input-top-text">Title</p>
        <input class="aidex-input clean" type="text" placeholder="Title" value="<?php echo $mainTitle->getName();?>" id="update_title" name="update_title" required>        
    </div> 

    <div class="clear"></div>

    <input class="aidex-input clean" type="hidden" value="<?php echo $mainTitle->getId();?>" id="title_id" name="title_id" readonly>   

    <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>

    </form>

</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>