<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit User | Property" />
<title>Edit User | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Edit User Details</h2> 
    
    <div class="clear"></div>

        <?php
        if(isset($_POST['user_uid']))
        {
        $conn = connDB();
        $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
        ?>

            <form method="POST" action="utilities/editUserDetailsFunction.php">

                <div class="dual-input">
                    <p class="input-top-text">Name</p>
                    <input class="aidex-input clean" type="text" placeholder="Name" value="<?php echo $userDetails[0]->getUsername();?>" id="update_username" name="update_username" required>        
                </div> 

                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Email</p>
                    <input class="aidex-input clean"  type="email" placeholder="Email" value="<?php echo $userDetails[0]->getEmail();?>" id="update_email" name="update_email" required>        
                </div> 

                <div class="clear"></div>

                <div class="dual-input">
                    <p class="input-top-text">Contact</p>
                    <input class="aidex-input clean"  type="text" placeholder="Contact" value="<?php echo $userDetails[0]->getPhoneNo();?>" id="update_phone" name="update_phone" required>  
                </div>  

                <input class="aidex-input clean"  type="hidden" placeholder="Contact" value="<?php echo $userDetails[0]->getUid();?>" id="user_uid" name="user_uid" readonly>  

                <div class="clear"></div>

                <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>

            </form>

        <?php
        }
        ?>

    <div class="clear"></div>
       
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>