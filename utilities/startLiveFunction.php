<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Liveshare.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $liveId = rewrite($_POST["data_id"]);
    $status = "Available";
    $type = "1";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $liveIdDetails = getLiveShare($conn," id = ?   ",array("id"),array($liveId),"s");   

    if(!$liveIdDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "i";
        }

        array_push($tableValue,$liveId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"live_share"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "DELETED !!";
            header('Location: ../adminDashboard.php');
            // header('Location: ' . $_SERVER['HTTP_REFERER']);
            // exit;
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }

}
else 
{
    header('Location: ../index.php');
}
?>
