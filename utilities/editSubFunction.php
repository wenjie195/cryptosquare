<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Subshare.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $updateTitle = rewrite($_POST["update_title"]);
    $updateHost = rewrite($_POST["update_host"]);
    $updatePlatform = rewrite($_POST["update_platform"]);
    $updateLink = rewrite($_POST["update_link"]);
    $updateRemark = rewrite($_POST["update_remark"]);

    $updateHostTwo = rewrite($_POST["update_host_two"]);
    $updatePlatformTwo = rewrite($_POST["update_platform_two"]);
    $updateLinkTwo = rewrite($_POST["update_link_two"]);
    $updateRemarkTwo = rewrite($_POST["update_remark_two"]);

    $subUid = rewrite($_POST["sub_uid"]);

    // $allSub = getSubShare($conn," WHERE title = ? AND link = ? ",array("title","link"),array($updateTitle,$updateLink),"ss");
    // $existingSub = $allSub[0];

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $subIdDetails = getSubShare($conn," uid = ?   ",array("uid"),array($subUid),"s");   
    
    // if(!$existingSub)
    // if(!$subId)
    // {

    if(!$subIdDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($updateTitle)
        {
            array_push($tableName,"title");
            array_push($tableValue,$updateTitle);
            $stringType .=  "s";
        }
        if($updateHost)
        {
            array_push($tableName,"host");
            array_push($tableValue,$updateHost);
            $stringType .=  "s";
        }
        if($updatePlatform)
        {
            array_push($tableName,"platform");
            array_push($tableValue,$updatePlatform);
            $stringType .=  "s";
        }
        if($updateLink)
        {
            array_push($tableName,"link");
            array_push($tableValue,$updateLink);
            $stringType .=  "s";
        }
        if($updateRemark)
        {
            array_push($tableName,"remark");
            array_push($tableValue,$updateRemark);
            $stringType .=  "s";
        }

        if($updateHostTwo)
        {
            array_push($tableName,"host_two");
            array_push($tableValue,$updateHostTwo);
            $stringType .=  "s";
        }
        if($updatePlatformTwo)
        {
            array_push($tableName,"platform_two");
            array_push($tableValue,$updatePlatformTwo);
            $stringType .=  "s";
        }
        if($updateLinkTwo)
        {
            array_push($tableName,"link_two");
            array_push($tableValue,$updateLinkTwo);
            $stringType .=  "s";
        }
        if($updateRemarkTwo)
        {
            array_push($tableName,"remark_two");
            array_push($tableValue,$updateRemarkTwo);
            $stringType .=  "s";
        }

        array_push($tableValue,$subUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"sub_share"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "UPDATED !!";
            header('Location: ../adminDashboard.php');
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }

    // }
    // else
    // {
    //     echo "Duplicated";
    // }

}
else 
{
    header('Location: ../index.php');
}
?>
