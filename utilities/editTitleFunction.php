<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Title.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $title = rewrite($_POST["update_title"]);
    $id = rewrite($_POST["title_id"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $titleDetails = getTitle($conn," id = ?   ",array("id"),array($id),"i");   

    if(!$titleDetails)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($title)
        {
            array_push($tableName,"name");
            array_push($tableValue,$title);
            $stringType .=  "s";
        }
        array_push($tableValue,$id);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"title"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // echo "UPDATED !!";
            header('Location: ../adminDashboard.php');
        }
        else
        {
            echo "FAIL !!";
        }
    }
    else
    {
        echo "GG !!";
    }
}
else 
{
    header('Location: ../index.php');
}
?>