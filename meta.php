
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale = 1.0; 
maximum-scale=1.0; user-scalable=no" /> 
<meta name="author" content="Crypto Square">
<meta property="og:image" content="https://vidatechft.com/hosting-picture/crypto-square-meta.jpg" />
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>