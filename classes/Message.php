<?php
class Message {
    /* Member variables */
    var $id, $uid, $messageUid, $receiveSMS, $replySMS, $replyOne, $replyTwo, $replyThree, $userStatus, $adminStatus, $dateCreated, $dateUpdated;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
        
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getMessageUid()
    {
        return $this->messageUid;
    }

    /**
     * @param mixed $messageUid
     */
    public function setMessageUid($messageUid)
    {
        $this->messageUid = $messageUid;
    }

    /**
     * @return mixed
     */
    public function getReceiveSMS()
    {
        return $this->receiveSMS;
    }

    /**
     * @param mixed $receiveSMS
     */
    public function setReceiveSMS($receiveSMS)
    {
        $this->receiveSMS = $receiveSMS;
    }

    /**
     * @return mixed
     */
    public function getReplySMS()
    {
        return $this->replySMS;
    }

    /**
     * @param mixed $replySMS
     */
    public function setReplySMS($replySMS)
    {
        $this->replySMS = $replySMS;
    }

    /**
     * @return mixed
     */
    public function getReplyOne()
    {
        return $this->replyOne;
    }

    /**
     * @param mixed $replyOne
     */
    public function setReplyOne($replyOne)
    {
        $this->replyOne = $replyOne;
    }

    /**
     * @return mixed
     */
    public function getReplyTwo()
    {
        return $this->replyTwo;
    }

    /**
     * @param mixed $replyTwo
     */
    public function setReplyTwo($replyTwo)
    {
        $this->replyTwo = $replyTwo;
    }

    /**
     * @return mixed
     */
    public function getReplyThree()
    {
        return $this->replyThree;
    }

    /**
     * @param mixed $replyThree
     */
    public function setReplyThree($replyThree)
    {
        $this->replyThree = $replyThree;
    }

    /**
     * @return mixed
     */
    public function getUserStatus()
    {
        return $this->userStatus;
    }

    /**
     * @param mixed $userStatus
     */
    public function setUserStatus($userStatus)
    {
        $this->userStatus = $userStatus;
    }

    /**
     * @return mixed
     */
    public function getAdminStatus()
    {
        return $this->adminStatus;
    }

    /**
     * @param mixed $adminStatus
     */
    public function setAdminStatus($adminStatus)
    {
        $this->adminStatus = $adminStatus;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getMessage($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","message_uid","receive_message","reply_message","reply_one","reply_two","reply_three","user_status","admin_status","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"message");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $uid, $messageUid, $receiveSMS, $replySMS, $replyOne, $replyTwo, $replyThree, $userStatus, $adminStatus, $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new Message;
            $user->setId($id);
            $user->setUid($uid);
            $user->setMessageUid($messageUid);
            $user->setReceiveSMS($receiveSMS);
            $user->setReplySMS($replySMS);

            $user->setReplyOne($replyOne);
            $user->setReplyTwo($replyTwo);
            $user->setReplyThree($replyThree);

            $user->setUserStatus($userStatus);
            $user->setAdminStatus($adminStatus);
            
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
