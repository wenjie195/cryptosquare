<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Project Video | Property" />
<title>Edit Project Video  | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Project Video</h2>
        <div class="clear"></div>

        <form method="POST" action="utilities/editSubFunction.php">

            <?php
            if(isset($_POST['subdata_id']))
            {
                $conn = connDB();
                $subDetails = getSubShare($conn,"WHERE id = ? ", array("id") ,array($_POST['subdata_id']),"i");
            ?>
                <div class="width100 margin-top20">
                    <p class="input-top-text">Project Title</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getTitle();?>" name="update_title" id="update_title" required>       
                </div>
				<h4 class="margin-top30"><b>Project Video 1</b></h4>                
                <div class="dual-input">
                    <p class="input-top-text">Host One</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getHost();?>" name="update_host" id="update_host" required>       
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Platform One</p>

                    <select class="aidex-input clean" type="text" name="update_platform" id="update_platform" required>
                        <option value="">Please Select A Platform</option>
                        <?php
                        for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                        {
                        ?>
                            <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                            </option>
                        <?php
                        }
                        ?>
                    </select> 

                </div>
				<div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-text">Link One</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getLink();?>" name="update_link" id="update_link" required>       
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Remark One</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getRemark();?>" name="update_remark" id="update_remark">       
                </div>
                <div class="clear"></div>  
				<h4 class="margin-top30"><b>Project Video 2</b></h4>
                <div class="dual-input">
                    <p class="input-top-text">Host Two</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getHostTwo();?>" name="update_host_two" id="update_host_two">       
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Platform Two</p>                    
                    <select class="aidex-input clean" type="text" name="update_platform_two" id="update_platform_two">
                        <option value="">Please Select A Platform</option>
                        <?php
                        for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                        {
                        ?>
                            <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                            </option>
                        <?php
                        }
                        ?>
                    </select> 

                </div>
				<div class="clear"></div>  
                <div class="dual-input">
                    <p class="input-top-text">Link Two</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getLinkTwo();?>" name="update_link_two" id="update_link_two">       
                </div>

                <div class="dual-input second-dual-input">
                    <p class="input-top-text">Remark Two</p>
                    <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getRemarkTwo();?>" name="update_remark_two" id="update_remark_two">       
                </div>
				<div class="clear"></div>  
                <input type="hidden" value="<?php echo $subDetails[0]->getUid();?>" name="sub_uid" id="sub_uid" readonly> 

            <?php
            }
            ?>
    	
            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" type="submit" id ="submit" name ="submit">Submit</button>
            </div>

        </form>

	</div>


<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>