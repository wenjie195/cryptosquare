<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
// $userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="User Logo | Property" />
<title>User Logo | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">User Logo</h2> 
    
    <div class="clear"></div>

        <?php
        if(isset($_POST['user_uid']))
        {
        $conn = connDB();
        $userLogo = getImage($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['user_uid']),"s");
            if($userLogo)
            {
                for($cnt = 0;$cnt < count($userLogo) ;$cnt++)
                {
                ?>
                    <a href="https://ecoworld.my/ecoforest/" target="_blank"><img src="img/logo-03.png" class="first-logo project-logo opacity-hover"></a>
                    <a href="https://ecoworld.my/ecohorizon/" target="_blank"><img src="img/logo-04.png" class="second-logo project-logo opacity-hover"></a>
                <?php
                }
                ?>
            <?php
            }
        }
        ?>

    <div class="clear"></div>
       
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>