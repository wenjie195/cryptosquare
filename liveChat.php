<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Live Chat | Property" />
<title>Live Chat | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Live Chat</h2> 
    
    <div class="clear"></div>
    
    <form action="utilities/submitCSFunction.php" method="POST">

        <?php
        $conn = connDB();

        $liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
        // $title = $liveDetails[0]->getUsername(); 

        if($liveDetails)
        {
        ?>
        <?php
        }
        ?>

        <div class="dual-input">
            <p class="input-top-text">Message</p>
            <input class="aidex-input clean" type="text" placeholder="Your Message" name="message_details" id="message_details" required>       
        </div>

        <button class="clean-button clean login-btn pink-button" name="submit">Sent</button>
    </form>
       
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>