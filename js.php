<div class="footer-div width100 overflow text-center">
	<p class="footer-p">© <?php echo $time;?> Crypto Square, All Rights Reserved.</p>	
</div>



<!-- Login Modal -->
<div id="login-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-login">&times;</span>
        <div class="clear"></div>
   <h1 class="title-h1 gold-text login-h1">Login</h1>	   
   <div class="big-white-div">
		<div class="login-div">
         <form action="utilities/loginFunction.php" method="POST">
            <div class="login-input-div">
                <p class="input-top-text">Username</p>
                <input class="aidex-input clean" type="text" placeholder="Username" id="username" name="username" required>
            </div>  
            <div class="login-input-div">
                <p class="input-top-text">Password</p>
                <input class="aidex-input clean" type="password" placeholder="Password" id="password" name="password" required>
            </div>   

            <button class="clean-button clean login-btn pink-button" name="login">Login</button>
        </form>
          
      </div>                 
 </div>
        
                
	</div>

</div>
<!-- HomeVideo Modal -->
<div id="homevideo-modal" class="modal-css">
	<div class="modal-content-css login-modal-content">
        <span class="close-css close-homevideo">&times;</span>
        <div class="clear"></div>
       <h1 class="title-h1 gold-text login-h1">Home Main Video</h1>	   
       <div class="big-white-div">
           <img src="img/home-main.png" class="width100">              
       </div>
        
                
	</div>

</div>
<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
<script>

var loginmodal = document.getElementById("login-modal");
var homevideomodal = document.getElementById("homevideo-modal");

var openlogin = document.getElementsByClassName("open-login")[0];
var openhomevideo = document.getElementsByClassName("open-homevideo")[0];



var closelogin = document.getElementsByClassName("close-login")[0];
var closehomevideo = document.getElementsByClassName("close-homevideo")[0];

if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}

if(closelogin){
  closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}

window.onclick = function(event) {

  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }  

    
}
</script>
<script src="js/headroom.js"></script>
<script>
(function() {
    var header = new Headroom(document.querySelector("#header"), {
        tolerance: 5,
        offset : 205,
        classes: {
          initial: "animated",
          pinned: "slideDown",
          unpinned: "slideUp"
        }
    });
    header.init();

    var bttHeadroom = new Headroom(document.getElementById("btt"), {
        tolerance : 0,
        offset : 500,
        classes : {
            initial : "slide",
            pinned : "slide--reset",
            unpinned : "slide--down"
        }
    });
    bttHeadroom.init();
}());
</script>
<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>